import unittest
import numpy.testing as nptest
import numpy as np

from io import StringIO
from contextlib import redirect_stdout

import umi.simulate as sim
import umi.inout as io
import umi.utils as utils
import umi.top_editor as editor
import umi.leaflet_div as div

class TestUtils(unittest.TestCase):
    path = "tests/data/utils/"

    def tearDown(self):
        utils.run_silent("rm -f test.in test.out")

    def test_run_silent(self):
        out = StringIO()

        try:
            with redirect_stdout(out):
                utils.run_silent("ls")
        except:
            self.fail("Failed to run shell commands through subp")

        out = out.getvalue()
        self.assertEqual(out, "")

    def test_residue_size(self):
        utils.run_silent("cp %s/solvated.gro test.in" % self.path)

        POPC_size = utils.residue_size("test.in", "POPC")
        water_size = utils.residue_size("test.in", "SOL")

        self.assertEqual(POPC_size, 134)
        self.assertEqual(water_size, 3)

    def test_get_residue_array(self):
        utils.run_silent("cp %s/solvated_extraRes.gro test.in" % self.path)

        amounts = utils.get_residue_array("test.in")
        correct_amounts = [["POPC", 8], ["SOL", 380], ["POPC", 1], ["SOL", 1]]
        self.assertListEqual(amounts, correct_amounts)

    def test_get_residue_dict(self):
        utils.run_silent("cp %s/solvated_extraRes.gro test.in" % self.path)

        amounts = utils.get_residue_dict("test.in")
        correct_amounts = {"POPC" : 9, "SOL" : 381}
        self.assertDictEqual(amounts, correct_amounts)

    def test_get_rand_res_set(self):
        for i in range(100):
            res_set = utils.get_rand_res_set(1,200,10)

            self.assertGreaterEqual(min(res_set), 1)
            self.assertLessEqual(max(res_set), 200)
            self.assertEqual(len(res_set), 10)

        res_set = utils.get_rand_res_set(1,5,5)
        self.assertSetEqual(res_set, set([1,2,3,4,5]))

    def test_get_first_and_last_res(self):
        utils.run_silent("cp %s/solvated.gro test.in" % self.path)

        first, last = utils.get_first_and_last_res("test.in", "SOL")
        self.assertEqual(first, 9)
        self.assertEqual(last, 388)

        # Test that the function raise ValueError when no residues found
        with self.assertRaises(ValueError):
            utils.get_first_and_last_res("test.in", "ASD")

    def test_get_grid_square(self):
        square = utils.get_grid_square(0, 0, 0, [10, 10, 10], 10)
        self.assertTupleEqual(square, (0, 0, 0))

        square = utils.get_grid_square(9.9, 9.9, 9.9, [10, 10, 10], 10)
        self.assertTupleEqual(square, (9, 9, 9))

        square = utils.get_grid_square(4.0, 4.0, 4.0, [10, 10, 10], 10)
        self.assertTupleEqual(square, (4, 4, 4))

    def test_make_grid(self):
        utils.run_silent("cp %s/grid_test.gro test.in" % self.path)

        grid = utils.make_grid("test.in", 10)
        correct_grid = set([(0, 4, 1), (0, 0, 3)])

        grid = utils.make_grid("test.in", 5)
        correct_grid = set([(0, 2, 0), (0, 0, 1)])

        self.assertSetEqual(grid, correct_grid)

class TestInout(unittest.TestCase):
    path = "tests/data/inout/"

    def tearDown(self):
        utils.run_silent("rm -f test.in test.out")

    def test_fix_atoms(self):
        utils.run_silent("cp %s/POPC_wrong_num.gro test.in" % self.path)
        io.fix_atoms("test.in")

        self.assertEqual(io.get_gro_atom_num("test.in"), 134)

    def test_tail(self):
        tail = io.tail("%s/topol.top"%(self.path))
        self.assertEqual(tail, "POPC%s200\n" % (14 * " "))

    def test_lipid_size(self):
        size = io.lipid_size("topologies/gro/lipid.gro")
        nptest.assert_array_equal(size, [1.562, 1.562, 3.000])

    def test_dimensions(self):
        dim = io.dimensions("%s/POPC.gro"%(self.path))
        nptest.assert_array_equal(dim, [1.562, 1.562, 3.000])

    def test_split_gro_line(self):
        test_lines = [
            "  185SOL    HW1  554   1.441   1.210   1.732",
            "  326SOL    HW1  977   1.019   0.190   3.580",
            "  429SOL    HW1 1286   0.476   1.268   4.852",
            "  180POPC   C37  104   1.010   1.003   1.229   1.238   2.129   0.821"
        ]

        correct_res_num = [185, 326, 429, 180]
        correct_res_name = ["SOL", "SOL", "SOL", "POPC"]
        correct_a_name = ["HW1", "HW1", "HW1", "C37"]
        correct_a_num = [554, 977, 1286, 104]
        correct_x = [1.441, 1.019, 0.476, 1.010]
        correct_y = [1.210, 0.190, 1.268, 1.003]
        correct_z = [1.732, 3.580, 4.852, 1.229]

        for i,line in enumerate(test_lines):
            returns = io.split_gro_line(line)

            self.assertEqual(correct_res_num[i], returns[0])
            self.assertEqual(correct_res_name[i], returns[1])
            self.assertEqual(correct_a_name[i], returns[2])
            self.assertEqual(correct_a_num[i], returns[3])
            self.assertEqual(correct_x[i], returns[4])
            self.assertEqual(correct_y[i], returns[5])
            self.assertEqual(correct_z[i], returns[6])

    def test_get_gro_line(self):
        res_num = [185, 326, 429, 180]
        res_name = ["SOL", "SOL", "SOL", "POPC"]
        a_name = ["HW1", "HW1", "HW1", "C37"]
        a_num = [554, 977, 1286, 104]
        x = [1.441, 1.019, 0.476, 1.010]
        y = [1.210, 0.190, 1.268, 1.003]
        z = [1.732, 3.580, 4.852, 1.229]

        correct = [
            "  185SOL    HW1  554   1.441   1.210   1.732\n",
            "  326SOL    HW1  977   1.019   0.190   3.580\n",
            "  429SOL    HW1 1286   0.476   1.268   4.852\n",
            "  180POPC   C37  104   1.010   1.003   1.229\n"
        ]

        for i in range(len(res_num)):
            params = [
                res_num[i], res_name[i],
                a_name[i], a_num[i],
                x[i], y[i], z[i]
            ]

            self.assertEqual(io.get_gro_line(*params), correct[i])

    def test_get_gro_num(self):
        atoms = io.get_gro_atom_num("%s/POPC.gro"%(self.path))
        self.assertEqual(atoms, 134)

    def test_update_dimensions(self):
        utils.run_silent("cp %s/POPC.gro test.in" % self.path)

        lines = io.read_all_lines("test.in")
        io.update_dimensions("test.in", 1, 2, 3)
        lines_updated = io.read_all_lines("test.in")

        for i in range(len(lines)-1):
            self.assertEqual(lines[i], lines_updated[i])

        dims = io.dimensions("test.in")

        self.assertEqual(dims[0], 1)
        self.assertEqual(dims[1], 2)
        self.assertEqual(dims[2], 3)

class TestEditor(unittest.TestCase):
    path = "tests/data/editor/"

    def tearDown(self):
        utils.run_silent("rm -f test.in test.out")

    def test_get_res_name(self):
        files = [
            "CLA.itp",
            "POPC.itp",
            "POT.itp",
            "PROA.itp",
            "SAPI25.itp",
            "spc.itp"
        ]

        res_names = [
            "CLA",
            "POPC",
            "POT",
            "PROA",
            "SAPI25",
            "SOL"
        ]

        for i,fname in enumerate(files):
            res_name = editor.get_res_name("%s/%s" % (self.path, fname))
            self.assertEqual(res_name, res_names[i])

    def test_solvate(self):
        utils.run_silent("cp %s/memb.gro test.in" % self.path)

        water_num = editor.solvate("test.in", "test.out", 400, "POPC")
        res_dict = utils.get_residue_dict("test.out")

        self.assertEqual(res_dict["SOL"], water_num)
        self.assertGreater(water_num, 400)

    def test_get_ion_arrays(self):
        utils.run_silent("cp %s/solvated.gro test.in" % self.path)

        first_sol, last_sol = utils.get_first_and_last_res("test.in", "SOL")
        rm_sol = utils.get_rand_res_set(first_sol, last_sol, 80)

        pos_array, neg_array, added_ions = \
            editor.get_ion_arrays("test.in", 20, 10, rm_sol)

        # test pos_array
        for line in pos_array:
            res_num, res_name, a_name, a_num, x, y, z = io.split_gro_line(line)

            self.assertEqual(a_name, "O")
            self.assertIn(res_num, added_ions)

        # test neg_array
        for line in pos_array:
            res_num, res_name, a_name, a_num, x, y, z = io.split_gro_line(line)

            self.assertEqual(a_name, "O")
            self.assertIn(res_num, added_ions)

        self.assertEqual(len(pos_array) + len(neg_array), len(added_ions))
        self.assertEqual(len(added_ions), 30)

    def test_add_ions(self):
        utils.run_silent("cp %s/solvated.gro test.in" % self.path)

        editor.add_ions("test.in", "test.out", 300, conc=0.15)
        res_dict = utils.get_residue_dict("test.out")

        charge = res_dict["POT"] - res_dict["CLA"]
        self.assertEqual(charge, 0)

        editor.add_ions("test.in", "test.out", 300, charge=1, conc=0.15)
        res_dict = utils.get_residue_dict("test.out")

        ions_wanted = int(np.round(0.15/55.5*300))
        self.assertEqual(res_dict["POT"], ions_wanted)

        with self.assertRaises(KeyError):
            res_dict["CLA"]

        charge = res_dict["POT"]
        self.assertEqual(charge, 1)

    def test_rm_random(self):
        utils.run_silent("cp %s/memb.gro test.in" % self.path)

        for i in range(5):
            editor.rm_random("test.in", "test.out", "POPC", "P", 3, 2)
            upper, lower = div.get_leaflet_sets("test.out", "POPC", "P")

            self.assertEqual(len(upper), 3)
            self.assertEqual(len(lower), 2)

            editor.rm_random("test.in", "test.out", "POPC", "P", 4, 0)
            upper, lower = div.get_leaflet_sets("test.out", "POPC", "P")

            self.assertEqual(len(upper), 4)
            self.assertEqual(len(lower), 0)

        with self.assertRaises(editor.MembTooSmallError):
            editor.rm_random("test.in", "test.out", "POPC", "P", 10, 0)

if __name__ == "__main__":
    unittest.main(verbosity=2)
