![Scheme](umi.png)

# Unlimited Membrane Interface — UMI

## Installation
All the required packages can be easily installed through [Anaconda](https://docs.anaconda.com/anaconda/install/linux/).

```
conda install -c omnia openmm
conda install -c omnia parmed
```

Test openmm installation.

```
python3 -m simtk.testInstallation
```

Clone the repository.

```
git clone https://bitbucket.org/Kegi91/umi.git
```

Add the repository to PYTHONPATH env variable and test that it is working by running the unit tests.

```
cd umi/
export PYTHONPATH="./"
pytest -v
```
