import os
import numpy as np
import subprocess as subp
import parmed as pmd

def convert_coordinates(in_file, out_file):
    pmd.load_file(in_file, skip_bonds=True).save(out_file, overwrite=True)

# Fix gro file atom number
def fix_atoms(fname):
    atoms = int(subp.getoutput("wc -l < %s"%fname)) - 3
    subp.run(["sed", "-i", "2s/.*/%d/"%atoms, fname])

def read_all_lines(fname):
    f_in = open(fname, 'r')
    lines = f_in.readlines()
    f_in.close()

    return lines

def tail(f):
    if not os.path.isfile(f):
        raise IOError("File %s not found" % f)

    return subp.getoutput('tail -1 ' + f) + "\n"

def skip_lines(f, i):
    for i in range(i):
        next(f)

def write_lines(f_in, f_out, i):
    for i in range(i):
        f_out.write(f_in.readline())

def cat_gro_files(fname_gro1, fname_gro2, fname_out):
    gro1 = open(fname_gro1, 'r')
    gro2 = open(fname_gro2, 'r')
    f_out = open(fname_out, 'w')

    gro1.readline()
    atoms1 = int(gro1.readline())

    f_out.write(gro2.readline())
    atoms2 = int(gro2.readline())

    f_out.write("%d\n" % (atoms1 + atoms2))

    # Write gro1 coords to f_out
    for i in range(atoms1):
        line = gro1.readline()
        f_out.write(line)

        if i == (atoms1 - 1):
            res_num, res_name, a_name, a_num, x, y, z = split_gro_line(line)

    res_offset = res_num
    atom_offset = a_num

    # Write gro2 coords to f_out with updated indexes
    for i in range(atoms2):
        line = gro2.readline()
        res_num, res_name, a_name, a_num, x, y, z = split_gro_line(line)

        res_num += res_offset
        a_num += atom_offset

        f_out.write(
            get_gro_line(
                res_num, res_name, a_name, a_num, x, y, z
            )
        )

    f_out.write(gro2.readline())

    gro1.close()
    gro2.close()
    f_out.close()

def lipid_size(lipid_coords):
    size = tail(lipid_coords)
    split = size.split()

    x = float(split[0])
    y = float(split[1])
    z = float(split[2])

    return np.array([x,y,z])

def dimensions(f_in):
    size = tail(f_in)
    split = size.split()

    x = float(split[0])
    y = float(split[1])
    z = float(split[2])

    return np.array([x,y,z])

def dimensions_pdb(f_in):
    with open(f_in) as pdb:
        for line in pdb:
            split = line.split()
            if split[0] == "CRYST1":
                x = float(split[1])
                y = float(split[2])
                z = float(split[3])
                return np.array([x,y,z]) / 10

    raise Exception("pdb dimensions could not be read")

def split_gro_line(line):
    res_num = int(line[0:5])
    res_name = line[5:10].strip()
    a_name = line[10:15].strip()
    a_num = int(line[15:20])
    x = float(line[20:28])
    y = float(line[28:36])
    z = float(line[36:44])

    return res_num, res_name, a_name, a_num, x, y, z

def get_gro_line(res_num, res_name, a_name, a_num, x, y, z):
    return "%5d%-5s%5s%5d%8.3f%8.3f%8.3f\n" % (
        res_num, res_name, a_name, a_num, x, y, z
    )

def get_gro_atom_num(gro):
    with open(gro, 'r') as f_in:
        f_in.readline()
        atoms = int(f_in.readline())

    return atoms

def write_coords_not_in_set(fname_in, fname_out, rm_set):
    res_removed = set()

    atom_offset = 0
    res_offset = 0

    f_in = open(fname_in, 'r')
    f_out = open(fname_out, 'w')

    f_out.write(f_in.readline())

    line = f_in.readline()
    f_out.write(line)
    atoms = int(line)

    for i in range(atoms):
        line = f_in.readline()
        res, res_name, a_name, atom, x, y, z = split_gro_line(line)

        # Check if res is to be skipped and update offsets
        if res in rm_set:
            atom_offset += 1

            if res not in res_removed:
                res_removed.add(res)
                res_offset += 1

            continue

        f_out.write(
            get_gro_line(
                res - res_offset, res_name, a_name, atom - atom_offset, x, y, z
            )
        )

    # Write dimensions
    f_out.write(f_in.readline())

    f_in.close()
    f_out.close()

    fix_atoms(fname_out)

def write_single_lipid(f_out, lipid_a, lipid_coords, coords, res, atom, buff):
    dims = lipid_size(lipid_coords)

    x_old = dims[0]
    y_old = dims[1]
    z_old = dims[2]

    for line in lipid_a:
        res_num, res_name, a_name, a_num, x, y, z = split_gro_line(line)

        dx = x_old*coords[0]
        dy = y_old*coords[1]
        dz = z_old*coords[2] + buff

        if coords[2] == 1:
            f_out.write(
                get_gro_line(
                    res, res_name,
                    a_name, atom % 100000,
                    x+dx, y+dy, z+dz
                )
            )
        else:
            f_out.write(
                get_gro_line(
                    res, res_name,
                    a_name, atom % 100000,
                    x+dx, y+dy, (z_old-z)+dz
                )
            )

        atom += 1

# Read the lipid into a numpy array
def read_lipid(lipid_coords):
    f_in = open(lipid_coords)
    skip_lines(f_in, 2)
    lipid = np.array([])

    for line in f_in:
        lipid = np.append(lipid, line)

    lipid = np.delete(lipid, len(lipid)-1)

    f_in.close()
    return lipid

def update_dimensions(fname, x, y, z):
    lines = read_all_lines(fname)

    f_out = open(fname, 'w')
    for i in range(len(lines)-1):
        f_out.write(lines[i])

    f_out.write("%10.5f%10.5f%10.5f\n"%(x, y, z))
    f_out.close()
