from sys import stdout

# OpenMM Imports
import simtk.openmm.app as app
import simtk.openmm as mm
import simtk.unit as unit

import numpy as np
import matplotlib.pyplot as plt

import umi.utils as utils

def restrain_z(pdb, system, rest_a, rest_res, atoms, residues):
    force = mm.CustomExternalForce("k*((z-z0)^2)")

    force.addGlobalParameter(
        "k",
        5.0 * unit.kilocalories_per_mole / unit.angstroms**2
    )
    force.addPerParticleParameter("z0")

    for i, atom_crd in enumerate(pdb.getPositions()):
        atom = atoms[i]
        residue = residues[i]

        if atom in rest_a and residue in rest_res:
            force.addParticle(i, [atom_crd[2]])

    system.addForce(force)

def restrain_xy(pdb, system, rest_a, rest_res, atoms, residues):
    force = mm.CustomExternalForce("k*((x-x0)^2+(y-y0)^2)")

    force.addGlobalParameter(
        "k",
        5.0 * unit.kilocalories_per_mole / unit.angstroms**2
    )
    force.addPerParticleParameter("x0")
    force.addPerParticleParameter("y0")

    for i, atom_crd in enumerate(pdb.getPositions()):
        atom = atoms[i]
        residue = residues[i]

        if atom in rest_a and residue in rest_res:
            force.addParticle(i, atom_crd[:2])

    system.addForce(force)

def restrain_all(pdb, system, rest_a, rest_res, atoms, residues, k=5.0):
    force = mm.CustomExternalForce("k*((x-x0)^2+(y-y0)^2+(z-z0)^2)")

    force.addGlobalParameter(
        "k",
        k * unit.kilocalories_per_mole / unit.angstroms**2
    )
    force.addPerParticleParameter("x0")
    force.addPerParticleParameter("y0")
    force.addPerParticleParameter("z0")

    for i, atom_crd in enumerate(pdb.getPositions()):
        atom = atoms[i]
        residue = residues[i]

        if "PROT" in residues and residue in utils.PROT_REST_SET:
            force.addParticle(i, [atom_crd[:3]])

    system.addForce(force)

# Energy minimization with restraints on z-axis
def squeeze(pdb, top, cutoff=1.2, rest_res=[], rest_a=[]):
    print("Squeezing the membrane\n")

    # Load the Gromacs files
    print('Loading files')
    pdb = app.PDBFile("%s.pdb"%pdb)
    top = app.GromacsTopFile(
        "%s.top"%top,
        unitCellDimensions=pdb.topology.getUnitCellDimensions()
    )

    # Create the OpenMM system
    print('Creating OpenMM System')
    system = top.createSystem(
        nonbondedMethod=app.CutoffPeriodic,
        nonbondedCutoff=cutoff* unit.nanometer,
        constraints = app.HBonds
    )

    # Create the integrator to do Langevin dynamics
    integrator = mm.LangevinIntegrator(
        300 * unit.kelvin,       # Temperature of heat bath
        1.0 / unit.picoseconds,  # Friction coefficient
        0.002 * unit.picosecond  # Time step
    )

    # Set restraints
    atoms = [atom.name for atom in pdb.topology.atoms()]
    residues = [atom.residue.name for atom in pdb.topology.atoms()]
    restrain_z(pdb, system, rest_a, rest_res, atoms, residues)

    if "PROT" in rest_res:
        restrain_all(pdb, system, [], ["PROT"], atoms, residues, k=500)

    # Pressure coupling
    barostat = mm.MonteCarloMembraneBarostat(
        1000.0 * unit.bar, 0 * unit.bar * unit.nanometer,
        300 * unit.kelvin,
        mm.MonteCarloMembraneBarostat.XYIsotropic,
        mm.MonteCarloMembraneBarostat.ZFixed
    )
    system.addForce(barostat)

    sim = app.Simulation(top.topology, system, integrator)

    # Set the particle positions
    sim.context.setPositions(pdb.positions)

    # Minimize the energy
    print('Minimizing energy')
    sim.minimizeEnergy(maxIterations=500)

    # Set up the reporters to report energies and coordinates every 5000 steps
    # sim.reporters.append(PDBReporter('output.pdb', 1000))
    sim.reporters.append(
        app.StateDataReporter(
            stdout, 5000, step=True, potentialEnergy=True,
            kineticEnergy=True, temperature=True, volume=True,
            density=True
        )
    )

    # # Write the trajectory
    # sim.reporters.append(PDBReporter('output/trajectory.pdb', 1000))

    # # Write the trajectory in binary format
    # sim.reporters.append(DCDReporter('output/trajectory.dcd', 1000))

    print("Running simulation")
    box_old = \
        pdb.topology.getUnitCellDimensions()[0].value_in_unit(unit.nanometer)

    # Run until box dimensions stabilize
    while True:
        sim.step(1000)

        x,y,z = sim.context.getState().getPeriodicBoxVectors()
        box = x[0].value_in_unit(unit.nanometer)
        d_box = box-box_old
        box_old = box

        # Checking the derivative
        if d_box >= 0:
            break

    # Update pbc box
    pbc_box = sim.context.getState().getPeriodicBoxVectors()
    new_box = pbc_box[0] + pbc_box[1] + pbc_box[2]
    sim.topology.setUnitCellDimensions(new_box)

    # Printing output file
    position = sim.context.getState(getPositions=True).getPositions()
    app.PDBFile.writeFile(
        sim.topology,
        position,
        open("output/squeeze.pdb", "w")
    )

    print("Done\n")

def flatten(pdb, top, cutoff=1.2, rest_res=[], rest_a=[]):
    print("Flattening the membrane\n")

    # Load the Gromacs files
    print('Loading files')
    pdb = app.PDBFile("%s.pdb"%pdb)
    top = app.GromacsTopFile(
        "%s.top"%top,
        unitCellDimensions=pdb.topology.getUnitCellDimensions()
    )

    # Create the OpenMM system
    print('Creating OpenMM System')
    system = top.createSystem(
        nonbondedMethod=app.CutoffPeriodic,
        nonbondedCutoff=cutoff*unit.nanometer,
        constraints = app.HBonds
    )

    # Create the integrator to do Langevin dynamics
    integrator = mm.LangevinIntegrator(
        300 * unit.kelvin,       # Temperature of heat bath
        1.0 / unit.picoseconds,  # Friction coefficient
        0.002 * unit.picosecond  # Time step
    )

    # Set restraints
    atoms = [atom.name for atom in pdb.topology.atoms()]
    residues = [atom.residue.name for atom in pdb.topology.atoms()]

    if "PROT" in rest_res:
        restrain_all(pdb, system, [], ["PROT"], atoms, residues, k=500)

    # Pressure coupling
    barostat = mm.MonteCarloMembraneBarostat(
        1000.0*unit.bar, 0*unit.bar*unit.nanometer,
        300*unit.kelvin,
        mm.MonteCarloMembraneBarostat.XYIsotropic,
        mm.MonteCarloMembraneBarostat.ZFree
    )
    system.addForce(barostat)

    sim = app.Simulation(top.topology, system, integrator)

    # Set the particle positions
    sim.context.setPositions(pdb.positions)

    # Minimize the energy
    print('Minimizing energy')
    sim.minimizeEnergy(maxIterations=500)

    # Set up the reporters to report energies and coordinates every 5000 steps
    sim.reporters.append(
        app.StateDataReporter(
            stdout, 5000, step=True, potentialEnergy=True,
            kineticEnergy=True, temperature=True, volume=True,
            density=True
        )
    )

    # # Write the trajectory
    # sim.reporters.append(PDBReporter('output/trajectory.pdb', 1000))

    # # Write the trajectory in binary format
    # sim.reporters.append(DCDReporter('output/trajectory.dcd', 1000))

    print("Running simulation")
    box_old = pdb.topology.getUnitCellDimensions()[2].value_in_unit(
        unit.nanometer
    )

    # Run until box dimensions stabilize
    while True:
        sim.step(1000)

        x,y,z = sim.context.getState().getPeriodicBoxVectors()
        box = z[2].value_in_unit(unit.nanometer)
        d_box = box-box_old
        box_old = box

        # Checking the derivative
        if d_box >= 0:
            break

    # Update pbc box
    pbc_box = sim.context.getState().getPeriodicBoxVectors()
    new_box = pbc_box[0] + pbc_box[1] + pbc_box[2]
    sim.topology.setUnitCellDimensions(new_box)

    # Printing output file
    position = sim.context.getState(getPositions=True).getPositions()
    app.PDBFile.writeFile(
        sim.topology,
        position,
        open("output/flatten.pdb", "w")
    )

    print("Done\n")

def equilibrate(
    pdb, top, cutoff=1.2, rest_res=[], rest_a=[],
    npt=False, dt=0.001, platform='default'
):
    if npt:
        print("Running NPT equilibration\n")
        pdbfile = "output/nvt.pdb"
    else:
        print("Running NVT equilibration\n")
        pdbfile = "output/ions.pdb"

    # Load the Gromacs files
    print('Loading files')
    pdb = app.PDBFile("%s.pdb"%pdb)
    top = app.GromacsTopFile(
        "%s.top"%top,
        unitCellDimensions=pdb.topology.getUnitCellDimensions(),
    )

    # Create the OpenMM system
    print('Creating OpenMM System')
    system = top.createSystem(
        nonbondedMethod=app.CutoffPeriodic,
        nonbondedCutoff=cutoff*unit.nanometer,
        constraints=app.HBonds
    )

    # Create the integrator to do Langevin dynamics
    integrator = mm.LangevinIntegrator(
        300*unit.kelvin,       # Temperature of heat bath
        1.0/unit.picoseconds,  # Friction coefficient
        dt*unit.picosecond  # Time step
    )

    # Set restraints
    atoms = [atom.name for atom in pdb.topology.atoms()]
    residues = [atom.residue.name for atom in pdb.topology.atoms()]
    restrain_z(pdb, system, rest_a, rest_res, atoms, residues)

    if "PROT" in rest_res:
        restrain_all(pdb, system, [], ["PROT"], atoms, residues, k=100)

    # Pressure coupling
    if npt:
        barostat = mm.MonteCarloMembraneBarostat(
            1000.0*unit.bar, 0*unit.bar*unit.nanometer,
            300*unit.kelvin,
            mm.MonteCarloMembraneBarostat.XYIsotropic,
            mm.MonteCarloMembraneBarostat.ZFree
        )
        system.addForce(barostat)

    # Possible platforms: default, CPU, CUDA, OpenCL
    if platform == 'default':
        sim = app.Simulation(top.topology, system, integrator)
    else:
        platform = mm.Platform.getPlatformByName(platform)
        sim = app.Simulation(top.topology, system, integrator, platform)

    # Set the particle positions
    sim.context.setPositions(pdb.positions)

    # Set up the reporters to report energies and coordinates every 5000 steps
    sim.reporters.append(
        app.StateDataReporter(
            stdout, 5000, step=True, potentialEnergy=True,
            kineticEnergy=True, temperature=True, volume=True,
            density=True
        )
    )

    # Minimize the energy
    print('Minimizing energy')
    sim.minimizeEnergy(maxIterations=10000)

    # # Write the trajectory
    # sim.reporters.append(PDBReporter('output/trajectory.pdb', 100))

    # # Write the trajectory in binary format
    # sim.reporters.append(DCDReporter('output/trajectory.dcd', 100))

    print("Running simulation")
    box_old = pdb.topology.getUnitCellDimensions()[2].value_in_unit(
        unit.nanometer
    )

    # Run until box dimensions stabilize
    while True:
        sim.step(20000)

        x,y,z = sim.context.getState().getPeriodicBoxVectors()
        box = z[2].value_in_unit(unit.nanometer)
        d_box = box-box_old
        box_old = box

        # Checking the derivative
        if d_box >= 0:
            break

    # Update pbc box
    pbc_box = sim.context.getState().getPeriodicBoxVectors()
    new_box = pbc_box[0] + pbc_box[1] + pbc_box[2]
    sim.topology.setUnitCellDimensions(new_box)

    if npt:
        outfile = "npt"
    else:
        outfile = "nvt"

    # Printing output file
    position = sim.context.getState(getPositions=True).getPositions()
    app.PDBFile.writeFile(
        sim.topology,
        position,
        open("output/%s.pdb"%outfile, "w")
    )

    print("Done\n")
