"""CMD line user interface for UMI"""
import argparse

import numpy as np

import umi.inout as io
import umi.simulate as sim
import umi.utils as utils
import umi.top_editor as editor

ITP = "topologies/itp/"
GRO = "topologies/gro/"
TOP = "topologies/top/"
OUT = "output/"

def create_memb(lipid_coords, lipids_wanted, lipid_n, protein=None):
    """Create membrane for with or without protein

    :param lipid_n: Size :code:`n x n` of the created membrane
    :param lipids_wanted: Tuple (int, int) of the upper and
                         lower leaflet lipid numbers
    :param protein: GRO coordinates of the protein to be inserted to the
                    membrane. None, if no protein is to be inserted.
    :returns: None
    """
    insert_prot = protein is not None
    lipid_name = utils.get_lipid_name(lipid_coords)
    editor.make_memb(lipid_n, 0.1, lipid_coords, "%s/memb.gro" % OUT)
    dims = io.dimensions("%s/memb.gro" % OUT)
    div_atom = utils.get_first_atom(lipid_coords)

    if insert_prot:
        editor.center_protein(protein, "%s/prot.gro" % OUT, dims)
        grid = utils.make_grid("%s/prot.gro" % OUT, 20)
        editor.rm_overlap(
            "%s/memb.gro" % OUT, "%s/memb_rm.gro" % OUT, grid, 20
        )

    # Make recursively larger membrane if too few lipids
    memb_fname = "memb_rm.gro" if insert_prot else "memb.gro"
    try:
        editor.rm_random(
            "%s/%s" % (OUT, memb_fname), "%s/memb_final.gro" % OUT,
            lipid_name, div_atom, *lipids_wanted
        )
        print()
    except editor.MembTooSmallError:
        create_memb(lipid_coords, lipids_wanted, lipid_n+1, protein=protein)
        return

    if insert_prot:
        io.cat_gro_files(
            "%s/prot.gro" % OUT,
            "%s/memb_final.gro" % OUT,
            "%s/memb_prot.gro" % OUT
        )

def membrane_only_sys(lipid_coords, lipids_wanted, **kwargs):
    """Create a membrane without a protein

    :param lipid_coords: Path to the GRO file of a single lipid.
    :param lipids_wanted: A tuple (int, int) of lipids wanted in upper and
                          lower leaflets correspondingly.
    :param conc: Ion concentration (mol/l).
    :param cutoff: Non-bonded interaction cutoff (nm).
    :param platform: Platform used for the OpenMM simulation. Default uses
                     all recourses of the fasted detected platform (usually
                     GPU if one is available).
    :returns: None
    """
    conc = kwargs["conc"]
    charge = kwargs["charge"]
    cation = kwargs["cation"]
    anion = kwargs["anion"]
    cutoff = kwargs["cutoff"]
    platform = kwargs["platform"]
    run = kwargs["run"]

    lipids = sum(lipids_wanted)
    start_dim = int(np.ceil(max(lipids_wanted)**0.5))
    lipid_name = utils.get_lipid_name(lipid_coords)

    create_memb(lipid_coords, lipids_wanted, start_dim)
    io.convert_coordinates("output/memb_final.gro", "output/memb.pdb")

    if not run:
        print("Created %smemb.pdb" % OUT)
        return

    print()
    editor.make_top_file(
        "%s/memb_final.gro" % OUT,
        "%s/topol.top" % TOP,
        ["%s/charmm36.itp" % ITP, "%s/lipid.itp" % ITP]
    )

    sim.squeeze(
        "output/memb",
        "topologies/top/topol",
        cutoff=cutoff,
        rest_res=[lipid_name],
        rest_a=["C218", "C316", "N"]
    )

    sim.flatten(
        "output/squeeze",
        "topologies/top/topol",
        cutoff=cutoff,
        rest_res=[lipid_name],
        rest_a=["C218", "C316", "N"]
    )

    io.convert_coordinates(
        "output/flatten.pdb",
        "output/flatten.gro"
    )

    buff = utils.check_buff("output/flatten.gro")
    editor.fix_buff("output/flatten.gro", "output/flatten_fix.gro", 0.2-buff)

    editor.solvate(
        "output/flatten_fix.gro",
        "output/solvated.gro",
        water + int(np.round(conc/55.5 * water)) + 20,
        utils.get_lipid_name(lipid_coords)
    )

    editor.add_ions(
        "output/solvated.gro",
        "output/ions.gro",
        water,
        conc=conc,
        charge=charge,
        negIon=anion,
        posIon=cation
    )

    editor.make_top_file(
        "output/ions.gro",
        "topologies/top/topol.top",
        ["charmm36.itp", "lipid.itp", "sol.itp", "cation.itp", "anion.itp"]
    )

    io.convert_coordinates(
        "output/ions.gro",
        "output/ions.pdb"
    )

    sim.equilibrate(
        "output/ions",
        "topologies/top/topol",
        cutoff=cutoff,
        rest_res=[lipid_name],
        rest_a=["C218", "C316", "N"],
        dt=0.001,
        platform=platform
    )

    sim.equilibrate(
        "output/nvt",
        "topologies/top/topol",
        cutoff=cutoff,
        rest_res=[],
        rest_a=[],
        npt=True,
        dt=0.002,
        platform=platform
    )

    print("Created %snpt.pdb and %stopol.top" % (OUT, TOP))

def protein_sys(prot_coords, lipid_coords, lipids_wanted, **kwargs):
    """Create a membrane with a protein.

    :param prot_coords: Path to the GRO file of the protein.
    :param lipid_coords: Path to the GRO file of a single lipid.
    :param lipids_wanted: A tuple (int, int) of lipids wanted in upper and lower
                         leaflets correspondingly.
    :param conc: Ion concentration (mol/l).
    :param cutoff: Non-bonded interaction cutoff (nm).
    :param platform: Platform used for the OpenMM simulation. Default uses
                     all recourses of the fasted detected platform (usually
                     GPU if one is available).
    :returns: None
    """
    conc = kwargs["conc"]
    charge = kwargs["charge"]
    cation = kwargs["cation"]
    anion = kwargs["anion"]
    cutoff = kwargs["cutoff"]
    platform = kwargs["platform"]
    run = kwargs["run"]
    water = kwargs["water"]
    lipids = sum(lipids_wanted)

    start_dim = int(np.ceil(max(lipids_wanted)**0.5))
    lipid_name = utils.get_lipid_name(lipid_coords)

    create_memb(lipid_coords, lipids_wanted, start_dim, protein=prot_coords)
    io.convert_coordinates("%s/memb_prot.gro" % OUT, "%s/memb_prot.pdb" % OUT)

    if not run:
        print("Created %smemb_prot.pdb" % OUT)
        return

    print()
    editor.make_top_file(
        "%s/memb_prot.gro" % OUT,
        "%s/topol.top" % TOP,
        [
            "%s/charmm36.itp" % ITP,
            "%s/protein.itp" % ITP,
            "%s/lipid.itp" % ITP
        ],
        prot="%s/protein.gro" % GRO
    )

    sim.squeeze(
        "%s/memb_prot" % OUT,
        "%s/topol" % TOP,
        cutoff=cutoff,
        rest_res=[lipid_name, "PROT"],
        rest_a=["C218", "C316", "N"]
    )

    sim.flatten(
        "%s/squeeze" % OUT,
        "%s/topol" % TOP,
        cutoff=cutoff,
        rest_res=[lipid_name, "PROT"],
        rest_a=["C218", "C316", "N"]
    )

    io.convert_coordinates(
        "output/flatten.pdb",
        "output/flatten.gro"
    )

    buff = utils.check_buff("output/flatten.gro")
    editor.fix_buff("output/flatten.gro", "output/flatten_fix.gro", 0.2-buff)

    editor.solvate(
        "output/flatten_fix.gro",
        "output/solvated.gro",
        water + int(np.round(conc/55.5 * water)) + 20,
        utils.get_lipid_name(lipid_coords),

    )

    editor.add_ions(
        "output/solvated.gro",
        "output/ions.gro",
        water,
        conc=conc,
        charge=charge,
        negIon=anion,
        posIon=cation
    )

    editor.make_top_file(
        "output/ions.gro",
        "topologies/top/topol.top",
        [
            "charmm36.itp",
            "protein.itp",
            "lipid.itp",
            "sol.itp",
            "cation.itp",
            "anion.itp"
        ],
        prot=True
    )

    io.convert_coordinates(
        "output/ions.gro",
        "output/ions.pdb"
    )

    sim.equilibrate(
        "output/ions",
        "topologies/top/topol",
        cutoff=cutoff,
        rest_res=[lipid_name, "PROT"],
        rest_a=["C218", "C316", "N"],
        dt=0.001,
        platform=platform
    )

    sim.equilibrate(
        "output/nvt",
        "topologies/top/topol",
        cutoff=cutoff,
        rest_res=["PROT"],
        rest_a=[],
        npt=True,
        dt=0.002,
        platform=platform
    )

    print("Created %snpt.pdb and %stopol.top" % (OUT, TOP))

def _parse_args(parser):
    """Parse command line arguments."""
    parser.add_argument(
        "--lipids", required=True, nargs=2,
        help="Number of lipids in each leaflet"
    )
    parser.add_argument(
        "--run", action="store_true",
        help="Run also equilibration"
    )
    parser.add_argument(
        "--water", help="Total number of water added [lipids*50]"
    )
    parser.add_argument(
        "--conc", default=0.15, help="Ion concentration (mol/l) [0.15]"
    )
    parser.add_argument(
        "--charge", default=0, help="Total charge of the solvent (e) [0]"
    )
    parser.add_argument(
        "--cation", default="POT", help="Cation atom"
    )
    parser.add_argument(
        "--anion", default="CLA", help="Anion atom"
    )
    parser.add_argument(
        "--protein", help="Path to the GRO file of protein coordinates"
    )
    parser.add_argument(
        "--cutoff", default=1.2, help="Non-bonded cutoff (nm) [1.2]"
    )
    parser.add_argument(
        "--platform", default="default",
        help="Platform to be used for OpenMM simulations"
    )

    args = parser.parse_args()

    return args


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Create and equilibrate protein membrane systems.'
    )
    args = _parse_args(parser)

    lipids_wanted = int(args.lipids[0]), int(args.lipids[1])
    water = args.water

    if water is None:
        water = (lipids_wanted[0] + lipids_wanted[1]) * 50
    else:
        water = int(water)

    if args.protein is not None:
        protein_sys(
            args.protein,
            "topologies/gro/lipid.gro",
            lipids_wanted,
            water=water,
            conc=float(args.conc),
            charge=int(args.charge),
            cation=args.cation,
            anion=args.anion,
            cutoff=float(args.cutoff),
            platform=args.platform,
            run=args.run
        )
    else:
        membrane_only_sys(
            "topologies/gro/lipid.gro",
            lipids_wanted,
            water=water,
            conc=float(args.conc),
            charge=int(args.charge),
            cation=args.cation,
            anion=args.anion,
            cutoff=float(args.cutoff),
            platform=args.platform,
            run=args.run
        )
