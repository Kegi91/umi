import numpy as np
import subprocess as subp
import parmed as pmd
import simtk.unit as unit
import random

from umi.inout import split_gro_line, skip_lines, dimensions, fix_atoms

PROT_RES_SET = {
    "ALA", "ARG", "ASN", "ASP", "ASX", "CYS", "GLU", "GLN",
    "GLX", "GLY", "HIS", "ILE", "LEU", "LYS", "MET", "PHE",
    "PRO", "SER", "THR", "TRP", "TYR", "VAL", "HSD"
}

# Run shell commands without output to stdout nor stderr
def run_silent(cmd):
    subp.run(cmd, shell=True, stdout=subp.DEVNULL)

def fix_topol(f_in_name, f_out_name, residue, fix, add=False):
    f_in = open("topologies/top/%s"%f_in_name, 'r')
    f_out = open("topologies/top/%s"%f_out_name, 'w')

    for line in f_in:
        split = line.split()

        if len(split) > 0 and split[0] == residue:
            f_out.write("%s\t\t%d\n"%(residue, fix))
        else:
            f_out.write(line)

    if add:
        f_out.write("%s\t\t%d\n"%(residue, fix))

    f_in.close()
    f_out.close()

def add_res_to_topol(f_in_name, f_out_name, res_names, res_nums):
    f_in = open("topologies/top/%s"%f_in_name, 'r')
    f_out = open("topologies/top/%s"%f_out_name, 'w')

    for line in f_in:
        f_out.write(line)

    for i in range(len(res_names)):
        f_out.write("%s\t\t%d\n"%(res_names[i], res_nums[i]))

    f_in.close()
    f_out.close()

def add_ions_topol(topol_in, topol_out, gro):
    fix_atoms(gro)
    f_gro = open(gro, "r")

    pot = 0
    cla = 0

    f_gro.readline()
    atoms = int(f_gro.readline())

    for i in range(atoms):
        line = f_gro.readline()
        res_num, res_name, a_name, a_num, x, y, z = split_gro_line(line)

        if a_name == "POT":
            pot += 1
        elif a_name == "CLA":
            cla += 1

    f_gro.close()

    add_res_to_topol(topol_in, topol_out, ["POT", "CLA"], [pot, cla])

def get_gro_line_coords(line):
    res_num, res_name, a_name, a_num, x, y, z = split_gro_line(line)
    return np.array([x,y,z])

def get_center(fname):
    f_in = open(fname, 'r')

    f_in.readline()
    atoms = int(f_in.readline())

    center = np.zeros(3)

    for i in range(atoms):
        line = f_in.readline()
        center += get_gro_line_coords(line)

    f_in.close()
    return center / atoms

def get_first_atom(fname):
    with open(fname, "r") as f_in:
        skip_lines(f_in, 2)
        line = f_in.readline()
        res_num, res_name, a_name, a_num, x, y, z = split_gro_line(line)

    return a_name

# Calculate residue size assuming residue numbering is correct
def residue_size(fname, res):
    with open(fname, 'r') as f_in:
        f_in.readline()
        atoms = int(f_in.readline())
        i = 0

        # Find first residue atom
        res_name = ""
        while res_name != res:
            line = f_in.readline()
            res_num, res_name, a_name, a_num, x, y, z = split_gro_line(line)
            i += 1

        # Check if the found atom is last one
        if i == atoms:
            f_in.close()
            return 1

        # Count residue atoms
        residue = res_num
        res_atoms = 0
        while res_num == residue:
            if res_atoms == atoms - 1:
                return atoms

            line = f_in.readline()
            res_num, res_name, a_name, a_num, x, y, z = split_gro_line(line)

            res_atoms += 1

    return res_atoms

def pop_random(rm_set):
    rand_key = random.sample(rm_set, 1)[0]
    rm_set.remove(rand_key)
    return rand_key

# Return array of residue amounts
def get_residue_array(fname, skip=0):
    f_in = open(fname, 'r')
    amounts = []
    res_sizes = {}

    f_in.readline()
    atoms = int(f_in.readline())

    # Skip lines not wanted for the res_array i.e. the protein
    skip_lines(f_in, skip)

    # Count all atoms of each residue
    for i in range(atoms-skip):
        line = f_in.readline()
        res_num, res_name, a_name, a_num, x, y, z = split_gro_line(line)

        if res_name in PROT_RES_SET:
            continue

        if res_name not in res_sizes:
            res_sizes[res_name] = residue_size(fname, res_name)

        # Check if new residue
        if not amounts or res_name != last_res:
            amounts.append([res_name, 1])
            last_res = res_name
        # Else add one atom to latest residue
        else:
            amounts[-1][1] += 1

    # Fix amounts to count the residues instead of atoms
    for i in range(len(amounts)):
        size = amounts[i][1] / res_sizes[amounts[i][0]]
        if size != int(size):
            raise ValueError("Atom and residue count does not match")

        amounts[i][1] = int(size)

    f_in.close()
    return amounts

def get_lipid_name(gro):
    res_list = get_residue_array(gro)
    if len(res_list) > 1:
        raise ValueError("lipid.gro contains multiple different residues")
    
    return res_list[0][0]

def get_first_and_last_res(gro, res):
    f_in = open(gro, 'r')
    f_in.readline()
    atoms = int(f_in.readline())

    first = 0
    last = 0

    # Find first and last occurance
    for i in range(atoms):
        line = f_in.readline()
        res_num, res_name, a_name, a_num, x, y, z = split_gro_line(line)

        if res_name == res:
            if first == 0:
                first = res_num

            last = res_num

    f_in.close()

    if first == 0 or last == 0:
        raise ValueError("No residues found")

    return first, last

def get_residue_dict(fname, skip=0):
    f_in = open(fname, 'r')
    res_dict = {}
    res_sizes = {}

    f_in.readline()
    atoms = int(f_in.readline())

    # Skip lines not wanted for the res_dict i.e. the protein
    skip_lines(f_in, skip)

    for i in range(atoms):
        line = f_in.readline()
        res_num, res_name, a_name, a_num, x, y, z = split_gro_line(line)

        if res_name in PROT_RES_SET:
            continue

        if res_name not in res_sizes:
            res_sizes[res_name] = residue_size(fname, res_name)
            res_dict[res_name] = 0

        res_dict[res_name] += 1

    for key in res_dict:
        size = res_dict[key] / res_sizes[key]
        if size != int(size):
            raise ValueError("Atom and residue count does not match")

        res_dict[key] = int(size)

    f_in.close()
    return res_dict

def check_buff(fname):
    dim = dimensions(fname)

    f_in = open(fname, 'r')
    f_in.readline()
    n = int(f_in.readline())

    min_z = dim[2]
    max_z = 0

    for i in range(n):
        line = f_in.readline()
        z = split_gro_line(line)[-1]

        if z < min_z:
            min_z = z
        elif z > max_z:
            max_z = z

    f_in.close()
    return (dim[2]-max_z + min_z)/2

def total_charge(system):
    forces = system.getForces()
    n = forces[0].getNumParticles()

    charge_sum = 0
    for i in range(n):
        charge, sigma, epsilon = forces[0].getParticleParameters(i)
        charge_sum += charge * 1/elementary_charge

    return charge_sum

# Get a single random residue that has not been used yet
def get_single_rand_res(min_res, max_res, rand_res, removed):
    rand = np.random.randint(low=min_res, high=max_res+1)
    while rand in rand_res or rand in removed:
        rand = np.random.randint(low=min_res, high=max_res+1)

    rand_res.add(rand)

def get_rand_res_set(min_res, max_res, n, removed=set()):
    rand_res = set()

    for i in range(n):
        get_single_rand_res(min_res, max_res, rand_res, removed)

    return rand_res

def get_grid_square(x, y, z, dims, n):
    i = int(x * n / dims[0])
    j = int(y * n / dims[1])
    k = int(z * n / dims[2])

    return (i,j,k)

def make_grid(struct, n):
    dims = dimensions(struct)
    grid = set()

    f_in = open(struct, 'r')
    f_in.readline()
    atoms = int(f_in.readline())

    for line in range(atoms):
        line = f_in.readline()
        res_num, res_name, a_name, a_num, x, y, z = split_gro_line(line)

        square = get_grid_square(x, y, z, dims, n)
        if square not in grid:
            grid.add(square)

    f_in.close()
    return grid

def plot_grid(grid, n):
    import matplotlib.pyplot as plt

    array = np.zeros(shape=[n,n])
    for idx in grid:
        array[idx[0]][idx[1]]=1

    plt.imshow(array, cmap=plt.get_cmap("Greys"), clim=[0,1])
    plt.show()
